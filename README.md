# Quest System

# Quests 
Quests are created and hold all information regarding it in the template section. Status and progress should be in this 
script as well.

# Quest Journal
The Quest Journal holds all of the quests. By default, it holds the current, completed and canceled quests. It is linked
to the Quest Pop Up. 
There are three buttons on the top of the journal which allow the player to move between the current, completed and canceled
quests. More can be added by extending. 
The journal contains a scroll view in which different buttons representing each quest can be placed. A QuestEntity script
exists and is connected to a button prefab that is the entry

# Quest Pop Up
The Quest pop up should be asosiated to the quest journal in some way, be it by references or delegates. It is in charge
of showcasing new quests to accept or reject, leave current quests and view more info on a specific quests
