﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuestSystemCore
{
    public class QuestSystemQuestCategory 
    {
        public static readonly QuestSystemQuestCategory completed = new QuestSystemQuestCategory("Completed");
        public static QuestSystemQuestCategory current = new QuestSystemQuestCategory("Current");
        public static QuestSystemQuestCategory failed = new QuestSystemQuestCategory("Failed");


        protected string value;

        public QuestSystemQuestCategory(string newNameValue) {
            if (newNameValue == null) {
                throw new Exception("Value for QuestSystemQuestCategoryEnum cant be null");
            }
            value = newNameValue;
        }

        public override string ToString()
        {
            return value;
        }

        public string GetValue() {
            return value;
        }

    }
}