﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuestSystemCore
{
    [CreateAssetMenu(fileName = "New Quest", menuName = "Quest System/QuestTemplate")]
    public class QuestTemplate : ScriptableObject
    {
        public Sprite questIcon;

        [Header("Quest Text Fields")]
        public string questName;
        public string questDescription;
        public string questVictoryText;
        public string questDefeatText;

        [Header("Quest Value Fields")]
        public int questLevel;
        public int questDifficulty;

    }
}