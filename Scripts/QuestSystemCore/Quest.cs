﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuestSystemCore
{
    public class Quest
    {
        protected QuestTemplate template;
        
        public Quest(QuestTemplate newTemplate)
        {
            template = newTemplate ?? throw new Exception("You can´t create a Quest without a template");
        }

        #region Getters
        public string GetQuestName()
        {
            return template.questName;
        }

        public Sprite GetQuestIcon()
        {
            return template.questIcon;
        }

        public string GetQuestDescription()
        {
            return template.questDescription;
        }

        public string GetQuestVictoryText()
        {
            return template.questVictoryText;
        }

        public string GetQuestDefeatText()
        {
            return template.questDefeatText;
        }

        public int GetQuestDificultyRating()
        {
            return template.questDifficulty;
        }

        public int GetQuestLevel()
        {
            return template.questLevel;
        }

        #endregion
    }
}