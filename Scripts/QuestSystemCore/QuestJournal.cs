﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuestSystemCore
{
    public class QuestJournal
    {
        public delegate void QuestResolvedEvent(Quest questSolved, bool success);
        public QuestResolvedEvent questResolvedEventHandler;

        public delegate void QuestJournalModificationEvent(List<Quest> newListDisplayed);
        public QuestJournalModificationEvent questJournalModificationEventHandler;

        protected int size;
        protected List<Quest> currentQuestSlots;
        protected List<Quest> completedQuests;
        protected List<Quest> failedQuests;

        #region constructor
        public QuestJournal(int journalStartingSize)
        {
            size = journalStartingSize;
            currentQuestSlots = new List<Quest>(size);
            completedQuests = new List<Quest>();
            failedQuests = new List<Quest>();

            for (int i = 0; i<size; i++) {
                currentQuestSlots.Add(null);
            }
        }
        #endregion

        #region Getters
        public int GetMaxAmountOfActiveQuests()
        {
           return size;
        }

        public List<Quest> GetQuests()
        {
            List<Quest> currentActiveQuests = new List<Quest>();
            for (int i = 0; i<size;i++) {
                if (currentQuestSlots[i]!= null) {
                    currentActiveQuests.Add(currentQuestSlots[i]);
                }
            }

            return currentActiveQuests;
        }
       
        public List<Quest> GetCompletedQuests()
        {
            List<Quest> completedQuestsReplica = new List<Quest>(completedQuests);
           return completedQuestsReplica;
        }

        public List<Quest> GetFailedQuests()
        {
            List<Quest> failedQuestsReplica = new List<Quest>(failedQuests);
            return failedQuestsReplica;
        }

        #endregion

        #region Overrideable modifer methods
        public virtual bool AddQuest(Quest questToAdd)
        {
            for (int i = 0; i<size; i++) {
                if (currentQuestSlots[i] == null) {
                    currentQuestSlots[i] = questToAdd;
                    if (questJournalModificationEventHandler == null)
                    {
                        Debug.LogWarning("You dont have anything connected to quest adding");
                        return true;
                    }
                    questJournalModificationEventHandler.Invoke(currentQuestSlots);
                    return true;
                }
            }
            return false;

        }

        public virtual bool RemoveQuest(Quest quest)
        {
            for (int i=0; i<size; i++) {
                if (currentQuestSlots[i] == quest) {
                    currentQuestSlots[i] = null;
                    if (questJournalModificationEventHandler == null)
                    {
                        Debug.LogWarning("You dont have anything connected to quest removing");
                        return true;
                    }
                    questJournalModificationEventHandler.Invoke(currentQuestSlots);
                    return true;
                }
            }
            return false;
        }

        public virtual void CompleteQuest(Quest questToComplete)
        {
            for (int i = 0; i < size; i++)
            {
                if (currentQuestSlots[i] == questToComplete)
                {
                    currentQuestSlots[i] = null;
                    completedQuests.Add(questToComplete);
                    if (questResolvedEventHandler == null) {
                        Debug.LogWarning("You dont have anything connected to quest resolutions");
                        return;
                    }
                    questResolvedEventHandler.Invoke(questToComplete,true);
                    return;
                }
            }

            throw new Exception("The Quest you are trying to complete is not in your quest log!");
            }

        public virtual void FailQuest(Quest questToFail)
        {
            for (int i = 0; i < size; i++)
            {
                if (currentQuestSlots[i] == questToFail)
                {
                    currentQuestSlots[i] = null;
                    failedQuests.Add(questToFail);
                    if (questResolvedEventHandler == null)
                    {
                        Debug.LogWarning("You dont have anything connected to quest resolutions");
                        return;
                    }
                    questResolvedEventHandler.Invoke(questToFail, false);
                    return;
                }
            }

            throw new Exception("The Quest you are trying to fail is not in your quest log!");

        }

        public virtual List<Quest> ListQuestsOfType(QuestSystemQuestCategory categoryClicked)
        {
            if (categoryClicked == QuestSystemQuestCategory.completed) {
                return GetCompletedQuests();
            }

            if (categoryClicked == QuestSystemQuestCategory.current)
            {
                return GetQuests();
            }

            if (categoryClicked == QuestSystemQuestCategory.failed)
            {
                return GetFailedQuests();
            }
            throw new Exception("No quests exist of that type");
        }

        #endregion
    }
}