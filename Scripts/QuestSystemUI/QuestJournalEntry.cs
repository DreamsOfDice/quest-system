﻿using QuestSystemCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace QuestSystemUI
{
    public class QuestJournalEntry : MonoBehaviour
    {
        public delegate void QuestEntrySelectedEvent(Quest questSelected);
        public QuestEntrySelectedEvent QuestEntrySelectedEventHandler;
        
        public Image questEntryImage;
        public Button questEntryMainButtonPrefab;
        public Text nameText;

        protected Quest questInEntry;

        public virtual void SetupQuestEntry(Quest newQuest) {
            questInEntry = newQuest;
            if (questEntryMainButtonPrefab == null || newQuest == null)
            {
                throw new Exception("You did not correctly set this up");
            }           
            questEntryMainButtonPrefab.onClick.AddListener(delegate { ViewQuest(); });
            nameText.text = questInEntry.GetQuestName();
        }

        public void DisableQuestEntry() {
            QuestEntrySelectedEventHandler = null;
            questEntryMainButtonPrefab.onClick.RemoveAllListeners();
        }

        public void ViewQuest() {
            if (QuestEntrySelectedEventHandler == null) {
                throw new Exception("You did not asign selecting this quest to anything");
            }
            QuestEntrySelectedEventHandler.Invoke(questInEntry);
        }

    }
}