﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using QuestSystemCore;
using UnityEngine;
using UnityEngine.UI;

namespace QuestSystemUI
{
    public class QuestJournalUI : MonoBehaviour
    {
        #region delegate area
        public delegate List<Quest> QuestTabClickedEvent(QuestSystemQuestCategory categoryClicked);
        public QuestTabClickedEvent questTabClickedEventHandler;

       
        #endregion

        public QuestJournalEntry questEntityPrefab;
        public GameObject contentHolder;
        public Text currentTabLabel;
        public QuestPopUp popUpQuestWindow;
        public List<Button> tabButtons;     

        protected List<QuestJournalEntry> entiryObjectsPool = new List<QuestJournalEntry>();


        private void PoolCurrentQuestEntities()
        {
            foreach (QuestJournalEntry g in contentHolder.transform.GetComponentsInChildren<QuestJournalEntry>()) {
                g.gameObject.SetActive(false);
                entiryObjectsPool.Add(g);
            }
        }

        public void CloseJournalUI() {

        }

        public virtual void SetupJournalUI(){
            Instantiate(popUpQuestWindow,gameObject.GetComponentInParent<Transform>());
            popUpQuestWindow.ClosePopUp();
            tabButtons[0].onClick.AddListener(delegate { SwitchTab(QuestSystemQuestCategory.current); });
            tabButtons[1].onClick.AddListener(delegate { SwitchTab(QuestSystemQuestCategory.completed); });
            tabButtons[2].onClick.AddListener(delegate { SwitchTab(QuestSystemQuestCategory.failed); });
            
        }

        public virtual void SwitchTab(QuestSystemQuestCategory val) {
            currentTabLabel.text = val.ToString() + " Quests";

            DisplayQuests(questTabClickedEventHandler.Invoke(val));
            

        }

        public void ViewQuest(Quest questToView) {

        }

        protected virtual void DisplayQuests(List<Quest> questsToDisplay) {
            PoolCurrentQuestEntities();
            foreach (Quest q in questsToDisplay) {
                if (q != null)
                {
                    if (entiryObjectsPool.Count == 0)
                    {
                        QuestJournalEntry tempEntityRef = Instantiate(questEntityPrefab, contentHolder.transform);
                        tempEntityRef.SetupQuestEntry(q);
                        //tempEntityRef.QuestEntrySelectedEventHandler+= popUpQuestWindow.
                    }
                    else
                    {
                        entiryObjectsPool[0].SetupQuestEntry(q);
                        entiryObjectsPool.RemoveAt(0);
                    }

                }
                

            }

        }

        public virtual void QuestJournalUIInitialization(QuestJournal newJournal)
        {
            questTabClickedEventHandler += newJournal.ListQuestsOfType;
            DisplayQuests(newJournal.GetQuests());
            newJournal.questJournalModificationEventHandler += DisplayQuests;
            popUpQuestWindow.questAcceptedEventHandler += newJournal.AddQuest;
            popUpQuestWindow.questRejectedEventHandler += newJournal.RemoveQuest;

        }
    }
}