﻿using QuestSystemCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace QuestSystemUI
{

    public class QuestPopUp : MonoBehaviour
    {
        #region delegates
        public delegate bool QuestAcceptedEvent(Quest questAccepted);
        public QuestAcceptedEvent questAcceptedEventHandler;

        public delegate bool QuestRejectedEvent(Quest questAccepted);
        public QuestRejectedEvent questRejectedEventHandler;
        #endregion

        public Text questTitleLabel;
        public Button leftInteractionButton;
        public Button rightInteractionButton;
        public Text questDescription;
        public Button closeButton;

        protected Quest currentQuest;

        protected void GuiStandardBuilder(Quest questData) {
            currentQuest = questData ?? throw new Exception("Cant view null quest");
            gameObject.SetActive(true);
            leftInteractionButton.onClick.RemoveAllListeners();
            leftInteractionButton.gameObject.SetActive(true);
            leftInteractionButton.gameObject.transform.GetComponentInChildren<Text>().text = "Cancel";

            leftInteractionButton.onClick.AddListener(delegate { ClosePopUp(); });

            rightInteractionButton.onClick.RemoveAllListeners();
            rightInteractionButton.gameObject.SetActive(true);
            rightInteractionButton.gameObject.transform.GetComponentInChildren<Text>().text = "Accept";
            rightInteractionButton.onClick.AddListener(delegate { AcceptQuest(); });

            closeButton.onClick.AddListener(delegate { ClosePopUp(); });
            questDescription.text = questData.GetQuestDescription();
            questTitleLabel.text = questData.GetQuestName();

        }

        public virtual void ViewQuestSetup(Quest questToView) {

            GuiStandardBuilder(questToView);
            gameObject.SetActive(true);
            leftInteractionButton.onClick.RemoveAllListeners();
            leftInteractionButton.gameObject.SetActive(false);

            rightInteractionButton.onClick.RemoveAllListeners();
            rightInteractionButton.gameObject.SetActive(true);
            rightInteractionButton.gameObject.transform.GetComponentInChildren<Text>().text = "ok";
            rightInteractionButton.onClick.AddListener(delegate { ClosePopUp(); });     

        }
              
        public virtual void QuestOfferSetup(Quest questToView)
        {
            GuiStandardBuilder(questToView);

            leftInteractionButton.onClick.RemoveAllListeners();
            leftInteractionButton.gameObject.transform.GetComponentInChildren<Text>().text = "Reject";
            leftInteractionButton.onClick.AddListener(delegate { RejectQuest(); });

        }

        public virtual void ClosePopUp() {
            gameObject.SetActive(false);

        }

        public virtual void AcceptQuest() {
            if (questAcceptedEventHandler == null) {
                throw new Exception("Popup has not been linked to anything");
            }
            questAcceptedEventHandler(currentQuest);
        }

        public virtual void RejectQuest() {
            if (questRejectedEventHandler == null)
            {
                throw new Exception("Popup has not been linked to rejection Handlong");
            }            
            questRejectedEventHandler(currentQuest);
        }

    }
}