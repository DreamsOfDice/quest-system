﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using QuestSystemCore;
using QuestSystemUI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace QuestSystemUIPlaytests
{
    public class QuestSystemUITests
    {
        QuestTemplate templateTest;
        QuestJournal testJournal;
        QuestJournalUI testUI;
        bool upkeepDone = false;

        [SetUp]
        public void Setup()
        {
            SceneManager.LoadScene("QuestSystemUITestScene");
            upkeepDone = false;
            SceneManager.sceneLoaded += Upkeep;
        }

        public void Upkeep(Scene scene, LoadSceneMode mode)
        {

            if (scene.name != "QuestSystemUITestScene")
            {
                return;
            }
            templateTest = Resources.Load<QuestTemplate>("QuestSystemTests/QuestTemplateTest1");
            testJournal = new QuestJournal(3);
            testJournal.AddQuest(new Quest(templateTest));
            testUI = GameObject.Find("QuestJournalUI").GetComponent<QuestJournalUI>();
            upkeepDone = true;

        }

        [UnityTest]
        public IEnumerator QuestJournalSetupTest()
        {

            testUI.QuestJournalUIInitialization(testJournal);
            Assert.AreEqual(testJournal.GetQuests().Count,testUI.contentHolder.transform.childCount);
            
            yield return null;
        }

        [UnityTest]
        public IEnumerator QuestJournalQuestEntrySetupTest()
        {
            
            testUI.QuestJournalUIInitialization(testJournal);
            Assert.AreEqual(testJournal.GetQuests()[0].GetQuestName(), testUI.contentHolder.transform.GetChild(0).GetComponent<QuestJournalEntry>().nameText.text);

            yield return null;
        }

        [UnityTest]
        public IEnumerator PopUpViewQuestTest()
        {
            testUI.QuestJournalUIInitialization(testJournal);
            testUI.SetupJournalUI();
            Assert.IsFalse(testUI.popUpQuestWindow.gameObject.activeSelf);

            testUI.popUpQuestWindow.ViewQuestSetup(testJournal.GetQuests()[0]);
            Assert.IsTrue(testUI.popUpQuestWindow.gameObject.activeSelf);
            Assert.AreEqual(testUI.popUpQuestWindow.questTitleLabel.text, testJournal.GetQuests()[0].GetQuestName());
            Assert.AreEqual(testUI.popUpQuestWindow.questDescription.text, testJournal.GetQuests()[0].GetQuestDescription());
            
            yield return null;
        }

        [UnityTest]
        public IEnumerator PopUpQuestOfferSetupTest()
        {
            testUI.QuestJournalUIInitialization(testJournal);
            testUI.SetupJournalUI();
            Assert.IsFalse(testUI.popUpQuestWindow.gameObject.activeSelf);

            testUI.popUpQuestWindow.ViewQuestSetup(testJournal.GetQuests()[0]);
            Assert.IsTrue(testUI.popUpQuestWindow.gameObject.activeSelf);
            Assert.AreEqual(testUI.popUpQuestWindow.questTitleLabel.text, testJournal.GetQuests()[0].GetQuestName());
            Assert.AreEqual(testUI.popUpQuestWindow.questDescription.text, testJournal.GetQuests()[0].GetQuestDescription());

            yield return null;
        }

        [UnityTest]
        public IEnumerator PopUpQuestOfferAcceptTest()
        {
            testUI.QuestJournalUIInitialization(testJournal);
            testUI.SetupJournalUI();

            testUI.popUpQuestWindow.QuestOfferSetup(new Quest(templateTest));
            testUI.popUpQuestWindow.AcceptQuest();
            yield return new WaitForSeconds(2);
            Assert.AreEqual(testUI.contentHolder.transform.childCount, 2);
            Assert.AreEqual(testJournal.GetQuests().Count,2);
            yield return null;
        }

        [UnityTest]
        public IEnumerator CompleteQuestTest()
        {
            
            yield return null;
        }

    }
}
