﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using QuestSystemCore;
using UnityEngine;
using UnityEngine.TestTools;

namespace QuestSystemEditorTests
{
    public class QuestSystemBaseTests
    {
        QuestTemplate templateTest;
        QuestJournal testJournal;
        [SetUp]
        public void Setup() {
            templateTest = Resources.Load<QuestTemplate>("QuestSystemTests/QuestTemplateTest1");
            testJournal = new QuestJournal(3);
        }

        // A Test behaves as an ordinary method
        [Test]
        public void CreateQuestTest()
        {
            Quest dummyQuest = new Quest(templateTest);
            Assert.AreEqual(dummyQuest.GetQuestName(),templateTest.questName);
            Assert.AreEqual(dummyQuest.GetQuestIcon(), templateTest.questIcon);
            Assert.AreEqual(dummyQuest.GetQuestDescription(), templateTest.questDescription);
            Assert.AreEqual(dummyQuest.GetQuestVictoryText(), templateTest.questVictoryText);
            Assert.AreEqual(dummyQuest.GetQuestDefeatText(), templateTest.questDefeatText);
            Assert.AreEqual(dummyQuest.GetQuestDificultyRating(), templateTest.questDifficulty);
            Assert.AreEqual(dummyQuest.GetQuestLevel(), templateTest.questLevel);
            
        }

        [Test]
        public void GenerateQuestJournalTest()
        {
            Quest dummyQuest = new Quest(templateTest);
            QuestJournal testJournalGenerationTest = new QuestJournal(3);

            Assert.AreEqual(0, testJournalGenerationTest.GetQuests().Count);
            Assert.AreEqual(3, testJournalGenerationTest.GetMaxAmountOfActiveQuests());

        }

        [Test]
        public void AcceptQuestTest()
        {
            Quest dummyQuest = new Quest(templateTest);
           

            Assert.AreEqual(0, testJournal.GetQuests().Count);

            Assert.IsTrue(testJournal.AddQuest(dummyQuest));
            Assert.AreEqual(1, testJournal.GetQuests().Count);

            Assert.IsTrue(testJournal.AddQuest(new Quest(templateTest)));
            Assert.AreEqual(2, testJournal.GetQuests().Count);

            Assert.IsTrue(testJournal.AddQuest(new Quest(templateTest)));
            Assert.IsFalse(testJournal.AddQuest(new Quest(templateTest)));

            Assert.AreEqual(dummyQuest, testJournal.GetQuests()[0]);

        }

        [Test]
        public void AbandonQuestTest()
        {
            Quest dummyQuest = new Quest(templateTest);
            
            Assert.AreEqual(0, testJournal.GetQuests().Count);

            Assert.IsTrue(testJournal.AddQuest(dummyQuest));
            Assert.AreEqual(1, testJournal.GetQuests().Count);

            Assert.IsTrue(testJournal.RemoveQuest(testJournal.GetQuests()[0]));

        }

        [Test]
        public void CompleteQuestTest()
        {
            Quest dummyQuest = new Quest(templateTest);
            Quest dummyQuest2 = new Quest(templateTest);

            Assert.IsTrue(testJournal.AddQuest(dummyQuest));
            testJournal.CompleteQuest(dummyQuest);
            Assert.AreEqual(dummyQuest, testJournal.GetCompletedQuests()[0]);
            try
            {
                testJournal.CompleteQuest(dummyQuest2);

            }
            catch {
                Assert.Pass("You dont have that quest, hence exception was generated as expected");
            }
            
        }

        [Test]
        public void FailQuestTest()
        {
            Quest dummyQuest = new Quest(templateTest);
            Quest dummyQuest2 = new Quest(templateTest);

            Assert.IsTrue(testJournal.AddQuest(dummyQuest));
            testJournal.FailQuest(dummyQuest);
            Assert.AreEqual(dummyQuest, testJournal.GetFailedQuests()[0]);
            try
            {
                testJournal.FailQuest(dummyQuest2);

            }
            catch
            {
                Assert.Pass("You dont have that quest, hence exception was generated as expected");
            }

        }

    }
}
